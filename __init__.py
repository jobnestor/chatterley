#!/usr/bin/env python3
# coding: utf-8
# Copyright 2017 © Lars Bahner
# Written by Lars Bahner <lars.bahner@gmail.com>
"""Module exports"""

from chatterley.channel import Channel

__all__ = [
    'Channel'
]
