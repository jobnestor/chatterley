#!/usr/bin/env python3
# coding: utf-8
# Copyright 2017 © Lars Bahner
# Written by Lars Bahner <lars.bahner@gmail.com>
"""Chatserver"""

from chatterley.channel import Channel

KANALER = []

def list_kanaler():
    """List alle kanaler, som er instantierte"""
    for kanal in KANALER:
        print('%s: %s' % (kanal.name, kanal.topic))

MINKANAL = Channel(name='pappas', topic='Løst prat')
KANALER.append(MINKANAL)
JOBSKANAL = Channel(name='jobs', topic='Viktig samtale')
KANALER.append(JOBSKANAL)

list_kanaler()
