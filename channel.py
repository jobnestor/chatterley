#!/usr/bin/env python3
# coding: utf-8
# Copyright 2017 © Lars Bahner
# Written by Lars Bahner <lars.bahner@gmail.com>
"""Definition of Channel object"""

class Channel(object):
    """Base class for Jobs chatserver channels"""

    def __init__(self, name=None, topic=None):
        """Initialize channels

        Arguments:
            <string:name> - name of the channel.
        """

        if topic is None:
            raise TypeError('Du *må* sette topic!')

        self.name = name
        self.topic = topic
        self._history = []

    def say(self, message):
        """Add message to channel"""

        self._history.append(message)

    def history(self, prefix=''):
        """Print chat history"""

        for message in self._history:
            print('%s%s' % (prefix, message))
